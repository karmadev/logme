package com.github.logger;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import com.github.karmadeb.logger.LogProvider;
import com.github.karmadeb.logger.LogProviderImpl;
import com.github.karmadeb.logger.Logger;
import com.github.karmadeb.logger.LoggerManager;
import com.github.karmadeb.logger.bukkit.BukkitLogProviderImpl;
import com.github.karmadeb.logger.bukkit.BukkitLoggerImpl;
import com.github.karmadeb.logger.bukkit.file.BukkitLogFileImpl;
import com.github.karmadeb.logger.file.LogFile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    private ServerMock server;

    @BeforeEach
    public void setUp() {
        server = MockBukkit.mock();
    }

    @Test
    public void testLoggerManagerDoesNotConflict() {
        assertDoesNotThrow(LoggerManager::getInstance);
    }

    @Test
    public void testThatLogProviderDoesNotCausesAnyProblem() {
        LoggerManager manager = LoggerManager.getInstance();

        assertDoesNotThrow(() -> manager.getProvider(LogProviderImpl.class));
    }

    @Test
    public void testThatLogProviderIsABukkitLogProvider() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);

        assertInstanceOf(BukkitLogProviderImpl.class, provider);
    }

    @Test
    public void testThatLogProviderLoggerDoesNotConflict() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);

        assertDoesNotThrow(() -> provider.getLogger());
    }

    @Test
    public void testThatLogProviderLoggerIsABukkitLogger() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);
        Logger logger = provider.getLogger();

        assertInstanceOf(BukkitLoggerImpl.class, logger);
    }

    @Test
    public void testThatLogFileDoesNotConflict() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);
        Logger logger = provider.getLogger();

        assertDoesNotThrow(logger::getAttachedFile);
    }

    @Test
    public void testThatLogFileIsABukkitLogFile() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);
        Logger logger = provider.getLogger();
        LogFile logFile = logger.getAttachedFile();

        assertInstanceOf(BukkitLogFileImpl.class, logFile);
    }

    /**
     * Test disable until I find why this fails on CI/CD
     * environments
     *
     */
    @Test
    public void testThatLoggerLog_and_LoggerFile_works() {
        LoggerManager manager = LoggerManager.getInstance();
        LogProvider provider = manager.getProvider(LogProviderImpl.class);
        Logger logger = provider.getLogger();
        LogFile logFile = logger.getAttachedFile();

        assertDoesNotThrow(() -> logger.info("Hello world!"));
        assertDoesNotThrow(() -> logFile.info("&bHello world!"));
    }

    @AfterEach
    public void tearDown() {
        MockBukkit.unmock();
    }
}
