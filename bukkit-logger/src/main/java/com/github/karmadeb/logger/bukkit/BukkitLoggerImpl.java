package com.github.karmadeb.logger.bukkit;

import com.github.karmadeb.logger.LogMessage;
import com.github.karmadeb.logger.Logger;
import com.github.karmadeb.logger.LoggerImpl;
import com.github.karmadeb.logger.bukkit.file.BukkitLogFileImpl;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.nio.file.Path;
import java.util.function.Function;

/**
 * Logger implementation
 */
public class BukkitLoggerImpl extends LoggerImpl {

    /**
     * Create the logger impl
     *
     * @param impl the impl loader
     * @param container the logger file container
     * @param name the logger name
     */
    BukkitLoggerImpl(final BukkitLogProviderImpl impl, final Path container, final String name) {
        super(impl, new BukkitLogFileImpl(container), name);
    }

    /**
     * Set the on-log function of the
     * logger
     *
     * @param consumer the logger consumer
     * @return the logger
     * @throws UnsupportedOperationException always
     */
    @Override
    protected Logger onLog(Function<LogMessage, LogMessage> consumer) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Get the log message format
     *
     * @return the log message format
     */
    @Override
    public String getLogFormat() {
        return "&8(&6%name%&7 - %level-color%%level-name%&r %time%&8)&7:&r %%";
    }

    /**
     * Get the log message format when
     * logging into the log file
     *
     * @return the log file format
     */
    @Override
    public String getLogFileFormat() {
        return super.getLogFileFormat();
    }

    /**
     * Set the log message format
     *
     * @param format the log message format
     * @throws UnsupportedOperationException if the logger implementation
     *                                       does not support a format change
     */
    @Override
    public void setLogFormat(final String format) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Set the log file message format
     *
     * @param format the log file message format
     * @throws UnsupportedOperationException if the logger implementation
     *                                       does not support a format change
     */
    @Override
    public void setLogFileFormat(final String format) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    @Override
    protected void logMessage(final LogMessage message) {
        String formatted = this.formatMessage(message, this.getLogFormat());
        String fileFormatted = this.formatMessage(message, this.getLogFileFormat());

        if (message.getLevel() >= this.getMinimumLevel())
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', formatted));

        this.getAttachedFile().append(message.getLevel(), fileFormatted);
        this.messages.add(message);
    }

    protected String formatMessage(final LogMessage finalMessage, final String format) {
        String gDate = super.getGregorianDate();
        String date = super.getDate();
        String time = super.getTime();
        String level = String.valueOf(finalMessage.getLevel());
        String levelName = impl.resolveLevel(finalMessage.getLevel());
        String levelColor = "&5";

        switch (levelName) {
            case "INFO":
                levelColor = "&e";
                break;
            case "WARNING":
                levelColor = "&a";
                break;
            case "SEVERE":
                levelColor = "&b";
                break;
            case "ERROR":
                levelColor = "&c";
                break;
        }

        return format.replace("%date%", date)
                .replace("%g-date%", gDate)
                .replace("%time%", time)
                .replace("%name%", name)
                .replace("%level-color%", levelColor)
                .replace("%level%", level)
                .replace("%level-name%", levelName)
                .replace("%%", finalMessage.getFormattedMessage());
    }
}