package com.github.karmadeb.logger.bukkit.file;

import com.github.karmadeb.logger.file.LogFileImpl;
import org.bukkit.ChatColor;

import java.io.IOException;
import java.nio.file.*;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class BukkitLogFileImpl extends LogFileImpl {

    /**
     * Create a new log file
     *
     * @param container the log file container
     */
    public BukkitLogFileImpl(final Path container) {
        super(container);
    }

    /**
     * Append a line to the log file
     *
     * @param level     the log level
     * @param message   the message
     * @param arguments the message arguments
     */
    @Override
    public void append(final byte level, final String message, final Object... arguments) {
        GregorianCalendar now = GregorianCalendar.from(ZonedDateTime.now());

        EXECUTOR.schedule(() -> {
            if (now.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)) {
                passDay();
                calendar = now;
            }

            if (!Files.exists(this.logFile))
                tryCreateFile(this.logFile);

            try {
                long size = Files.size(this.logFile);
                if (size >= MAX_FILE_SIZE)
                    passDay();

                Files.write(this.logFile,
                        String.format("%s%n",
                                ChatColor.stripColor(
                                        ChatColor.translateAlternateColorCodes('&', message)))
                                .getBytes(),
                        StandardOpenOption.APPEND,
                        StandardOpenOption.CREATE);
            } catch (IOException ex) {
                RuntimeException x = new RuntimeException();
                x.addSuppressed(ex);

                throw x;
            }
        }, 0, TimeUnit.MILLISECONDS);
    }
}