package com.github.karmadeb.logger.bukkit;

import com.github.karmadeb.logger.LogProviderImpl;
import com.github.karmadeb.logger.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Log provider implementation
 */
public class BukkitLogProviderImpl extends LogProviderImpl {

    private final static Map<String, Logger> loggerMap = new ConcurrentHashMap<>();
    private static BukkitLoggerImpl impl;

    /**
     * Create or retrieves a new logger
     * instance
     *
     * @param name the logger name
     * @return the logger
     */
    @Override
    public Logger getLogger(final String name) {
        return this.getLogger(Paths.get("plugins").resolve(name).resolve("logs"), name);
    }

    /**
     * Get the logger for the specified
     * class
     *
     * @param owner the class owning the
     *              logger
     * @return the class logger
     */
    @Override
    public Logger getLogger(final Class<?> owner) {
        String className = owner.getSimpleName();
        return this.getLogger(Paths.get("plugins").resolve(className).resolve("logs"), owner);
    }

    /**
     * Get the main logger
     *
     * @param logsContainer the logger file container
     * @return the logger
     */
    @Override
    public Logger getLogger(final Path logsContainer) {
        if (BukkitLogProviderImpl.impl == null)
            BukkitLogProviderImpl.impl = new BukkitLoggerImpl(this, logsContainer, "LogProviderImpl");

        return BukkitLogProviderImpl.impl;
    }

    /**
     * Create or retrieves a new logger
     * instance
     *
     * @param logsContainer the logger file container
     * @param name          the logger name
     * @return the logger
     */
    @Override
    public Logger getLogger(final Path logsContainer, final String name) {
        return loggerMap.computeIfAbsent(name, (e) -> new BukkitLoggerImpl(this, logsContainer, name));
    }

    /**
     * Get all the existing logger
     * instances
     *
     * @return the logger instances
     */
    @Override
    public Collection<Logger> getLoggers() {
        return Collections.unmodifiableCollection(
                BukkitLogProviderImpl.loggerMap.values()
        );
    }
}
