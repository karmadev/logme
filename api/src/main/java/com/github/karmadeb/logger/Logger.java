package com.github.karmadeb.logger;

import com.github.karmadeb.logger.file.LogFile;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Represents a logger
 */
@SuppressWarnings("unused")
public abstract class Logger extends LoggerComponent {

    public final static byte LEVEL_DEBUG = 1;
    public final static byte LEVEL_INFO = 2;
    public final static byte LEVEL_WARNING = 4;
    public final static byte LEVEL_SEVERE = 8;
    public final static byte LEVEL_ERROR = 16;

    /**
     * Set the on-log function of the
     * logger
     *
     * @param consumer the logger consumer
     * @return the logger
     * @throws UnsupportedOperationException if the logger implementation
     * does not support onLog function
     */
    protected abstract Logger onLog(final Function<LogMessage, LogMessage> consumer) throws UnsupportedOperationException;

    /**
     * Get the log message format
     *
     * @return the log message format
     */
    public abstract String getLogFormat();

    /**
     * Get the log message format when
     * logging into the log file
     *
     * @return the log file format
     */
    public abstract String getLogFileFormat();

    /**
     * Get the minimum log level
     *
     * @return the minimum log level
     */
    public byte getMinimumLevel() {
        return LEVEL_INFO;
    }

    /**
     * Set the minimum log level
     *
     * @param level the minimum log level
     * @throws UnsupportedOperationException if the implementation
     * does not implement the method
     */
    public void setMinimumLevel(final byte level) throws UnsupportedOperationException {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not allow the use of setMinimumLevel(byte)");
    }

    /**
     * Set the log message format
     *
     * @param format the log message format
     * @throws UnsupportedOperationException if the logger implementation
     * does not support a format change
     */
    public abstract void setLogFormat(final String format) throws UnsupportedOperationException;

    /**
     * Set the log file message format
     *
     * @param format the log file message format
     * @throws UnsupportedOperationException if the logger implementation
     * does not support a format change
     */
    public abstract void setLogFileFormat(final String format) throws UnsupportedOperationException;

    /**
     * Log a message
     *
     * @param level the message level
     * @param message the message to log
     * @param arguments the message arguments
     */
    public abstract void log(final byte level, final String message, final Object... arguments);

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     */
    public void log(final byte level, final Throwable error) {
        log(level, error, "An error occurred:");
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(level, message.get(), arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final Supplier<String> message, final Object... arguments) {
        log(level, message.get(), arguments);
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final String message, final Supplier<Object[]> arguments) {
        log(level, message, arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public abstract void log(final byte level, final Throwable error, final String message, final Object... arguments);

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final Throwable error, final Supplier<String> message, final Object... arguments) {
        log(level, error, message.get(), arguments);
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final Throwable error, final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(level, error, message.get(), arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void log(final byte level, final Throwable error, final String message, final Supplier<Object[]> arguments) {
        log(level, error, message, arguments.get());
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    public void log(final LogMessage message) {
        if (message.getException() != null) {
            log(message.getLevel(), message.getException(), message.getMessage(), message.getArguments());
        } else {
            log(message.getLevel(), message.getMessage(), message.getArguments());
        }
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    public void log(final Supplier<LogMessage> message) {
        log(message.get());
    }

    /**
     * Log a debug message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void debug(final String message, final Object... arguments) {
        log(LEVEL_DEBUG, message, arguments);
    }

    /**
     * Log a debug message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void debug(final Supplier<String> message, final Object... arguments) {
        log(LEVEL_DEBUG, message, arguments);
    }

    /**
     * Log a debug message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void debug(final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_DEBUG, message, arguments);
    }

    /**
     * Log a debug message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void debug(final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_DEBUG, message, arguments);
    }

    /**
     * Log an info message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void info(final String message, final Object... arguments) {
        log(LEVEL_INFO, message, arguments);
    }

    /**
     * Log an info message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void info(final Supplier<String> message, final Object... arguments) {
        log(LEVEL_INFO, message, arguments);
    }

    /**
     * Log an info message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void info(final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_INFO, message, arguments);
    }

    /**
     * Log an info message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void info(final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_INFO, message, arguments);
    }

    /**
     * Log a warning message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void warning(final String message, final Object... arguments) {
        log(LEVEL_WARNING, message, arguments);
    }

    /**
     * Log a warning message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void warning(final Supplier<String> message, final Object... arguments) {
        log(LEVEL_WARNING, message, arguments);
    }

    /**
     * Log a warning message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void warning(final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_WARNING, message, arguments);
    }

    /**
     * Log a warning message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void warning(final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_WARNING, message, arguments);
    }

    /**
     * Log a severe message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void severe(final String message, final Object... arguments) {
        log(LEVEL_SEVERE, message, arguments);
    }

    /**
     * Log a severe message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void severe(final Supplier<String> message, final Object... arguments) {
        log(LEVEL_SEVERE, message, arguments);
    }

    /**
     * Log a severe message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void severe(final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_SEVERE, message, arguments);
    }

    /**
     * Log a severe message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void severe(final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_SEVERE, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final String message, final Object... arguments) {
        log(LEVEL_ERROR, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Supplier<String> message, final Object... arguments) {
        log(LEVEL_ERROR, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_ERROR, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_ERROR, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Throwable error, final String message, final Object... arguments) {
        log(LEVEL_ERROR, error, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Throwable error, final Supplier<String> message, final Object... arguments) {
        log(LEVEL_ERROR, error, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Throwable error, final Supplier<String> message, final Supplier<Object[]> arguments) {
        log(LEVEL_ERROR, error, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void error(final Throwable error, final String message, final Supplier<Object[]> arguments) {
        log(LEVEL_ERROR, error, message, arguments);
    }

    /**
     * Log an error message
     *
     * @param error the error
     */
    public void error(final Throwable error) {
        log(LEVEL_ERROR, error, "An error occurred:");
    }

    /**
     * Get the logger attached
     * log file
     *
     * @return the attached log file
     */
    public abstract LogFile getAttachedFile();

    /**
     * Get the logger logged message
     *
     * @return the logged messages
     */
    public abstract Collection<LogMessage> getLogMessages();

    /**
     * Flush the log message collection. This method
     * effectively clears all the log messages present
     * in {@link #getLogMessages()}
     */
    public abstract void flush();
}