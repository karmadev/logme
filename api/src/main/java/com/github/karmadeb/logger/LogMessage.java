package com.github.karmadeb.logger;

/**
 * Represents the log message
 */
public interface LogMessage {

    /**
     * Get the logging level
     *
     * @return the logging level
     */
    byte getLevel();

    /**
     * Get the logged exception
     *
     * @return the exception
     */
    Throwable getException();

    /**
     * Get the logged message
     *
     * @return the message
     */
    String getMessage();

    /**
     * Get the message arguments
     *
     * @return the arguments
     */
    Object[] getArguments();

    /**
     * Get the formatted message
     *
     * @return the formatted message
     */
    default String getFormattedMessage() {
        StringBuilder builder = new StringBuilder(this.getMessage());
        Object[] arguments = this.getArguments();

        StringHelper.formatString(builder, arguments);
        return builder.toString();
    }
}
