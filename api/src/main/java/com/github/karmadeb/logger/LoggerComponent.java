package com.github.karmadeb.logger;

import java.util.function.Function;

/**
 * Represents a logger component
 */
@SuppressWarnings("unused")
public abstract class LoggerComponent {

    /**
     * Get the logger component
     * name
     *
     * @return the logger component name
     */
    public abstract String getName();

    protected static String errorToString(final Throwable error) {
        return errorToString(error, (element) -> String.format("%s#%s(%s:%s)", element.getClassName(),
                element.getMethodName(), element.getFileName(), element.getLineNumber()));
    }

    protected static String errorToString(final Throwable error, final Function<StackTraceElement, String> traceMap) {
        StringBuilder builder = new StringBuilder();

        Throwable[] suppressed = error.getSuppressed();
        StackTraceElement[] stack = error.getStackTrace();

        for (int i = 0; i < stack.length; i++) {
            StackTraceElement element = stack[i];
            if (element == null) continue;

            builder.append("\t").append(traceMap.apply(element));
            if (i != stack.length - 1)
                builder.append('\n');
        }

        if (suppressed != null && suppressed.length != 0) {
            builder.append('\n');
            for (int i = 0; i < suppressed.length; i++) {
                Throwable sub = suppressed[i];

                builder.append("--- SUPPRESSED ---");
                builder.append(errorToString(sub, traceMap));

                if (i != suppressed.length - 1)
                    builder.append('\n');
            }
        }

        return builder.toString();
    }
}
