package com.github.karmadeb.logger;

import java.util.*;

/**
 * Represents a logger manager
 */
public final class LoggerManager {

    private final static Collection<LogProvider> providers = new ArrayList<>();
    private final static LoggerManager INSTANCE = new LoggerManager();

    private static synchronized void load() {
        ServiceLoader<LogProvider> logProviderLoader = ServiceLoader.load(LogProvider.class);
        Iterator<LogProvider> logProviderIterator = logProviderLoader.iterator();
        List<LogProvider> logProviderList = new ArrayList<>();
        logProviderIterator.forEachRemaining(logProviderList::add);

        updateLogProviders(logProviderList);
    }

    private static synchronized void updateLogProviders(final List<LogProvider> providers) {
        List<LogProvider> toAdd = new ArrayList<>();

        mainLoop:
        for (LogProvider provider : providers) {
            if (provider == null) continue;

            for (LogProvider existing : LoggerManager.providers) {
                if (existing == null) continue;
                if (provider.getClass().equals(existing.getClass()))
                    continue mainLoop;
            }

            toAdd.add(provider);
        }

        LoggerManager.providers.addAll(toAdd);
    }

    /**
     * Create the logger manager
     */
    private LoggerManager() {
        load();
    }

    /**
     * Update the logger manager providers
     *
     * @return the logger manager
     */
    public LoggerManager update() {
        load();
        return this;
    }

    /**
     * Get the log provider
     *
     * @return the provider
     */
    public LogProvider getProvider() {
        return getProvider(LogProvider.class);
    }

    /**
     * Get the log provider
     *
     * @param type the provider type
     * @return the provider
     * @param <T> the provider type
     * @throws NullPointerException if the type is null
     * @throws IllegalStateException if there's no provider
     */
    public <T extends LogProvider> T getProvider(final Class<T> type) throws NullPointerException, IllegalStateException {
        if (type == null) throw new NullPointerException();

        for (LogProvider provider : LoggerManager.providers) {
            if (provider == null) continue;

            Class<?> providerType = provider.getClass();
            if (type.isAssignableFrom(providerType))
                return type.cast(provider);
        }

        throw new IllegalStateException("Could not find log provider for " + type.getSimpleName());
    }

    /**
     * Get the manager instance
     *
     * @return the manager instance
     */
    public static LoggerManager getInstance() {
        return LoggerManager.INSTANCE;
    }
}
