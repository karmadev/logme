package com.github.karmadeb.logger.file;

import com.github.karmadeb.logger.LoggerComponent;
import com.github.karmadeb.logger.LogMessage;
import com.github.karmadeb.logger.Logger;

import java.nio.file.Path;
import java.util.function.Supplier;

/**
 * Represents a log file
 */
@SuppressWarnings("unused")
public abstract class LogFile extends LoggerComponent {

    /**
     * Get the file path
     * 
     * @return the file path
     */
    public abstract Path getFile();
    
    /**
     * Append a line to the log file
     *
     * @param level the log level
     * @param message the message
     * @param arguments the message arguments
     */
    public abstract void append(final byte level, final String message, final Object... arguments);

    /**
     * Append a line to the log file
     *
     * @param level the log level
     * @param message the message
     * @param arguments the message arguments
     */
    public void append(final byte level, final Supplier<String> message, final Object... arguments) {
        append(level, message.get(), arguments);
    }

    /**
     * Append a line to the log file
     *
     * @param level the log level
     * @param message the message
     * @param arguments the message arguments
     */
    public void append(final byte level, final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(level, message.get(), arguments.get());
    }

    /**
     * Append a line to the log file
     *
     * @param level the log level
     * @param message the message
     * @param arguments the message arguments
     */
    public void append(final byte level, final String message, final Supplier<Object[]> arguments) {
        append(level, message, arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void append(final byte level, final Throwable error, final String message, final Object... arguments) {
        if (error == null) {
            append(level, message, arguments);
            return;
        }

        StringBuilder builder = new StringBuilder(message);

        int index = builder.indexOf("{}");
        int argIndex = 0;
        while (index != 0) {
            String replacement = "null";
            if (arguments != null && arguments.length > argIndex)
                replacement = String.valueOf(arguments[argIndex]);

            builder.replace(index, index + 1, replacement);
            index = builder.indexOf("{}");
        }

        builder.append('\n');
        builder.append(error.getMessage()).append('\n');
        builder.append(errorToString(error));

        append(level, builder.toString(), arguments);
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void append(final byte level, final Throwable error, final Supplier<String> message, final Object... arguments) {
        append(level, error, message.get(), arguments);
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void append(final byte level, final Throwable error, final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(level, error, message.get(), arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     * @param message the message to log
     * @param arguments the message arguments
     */
    public void append(final byte level, final Throwable error, final String message, final Supplier<Object[]> arguments) {
        append(level, error, message, arguments.get());
    }

    /**
     * Log a message
     *
     * @param level the message level
     * @param error the error
     */
    public void append(final byte level, final Throwable error) {
        append(level, error, "An error occurred:");
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    public void append(final LogMessage message) {
        append(message.getLevel(), message.getMessage(), message.getArguments());
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    public void append(final Supplier<LogMessage> message) {
        append(message.get());
    }

    /**
     * Append a debug message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void debug(final String message, final Object... arguments) {
        append(Logger.LEVEL_DEBUG, message, arguments);
    }

    /**
     * Append a debug message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void debug(final Supplier<String> message, final Object... arguments) {
        append(Logger.LEVEL_DEBUG, message, arguments);
    }

    /**
     * Append a debug message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void debug(final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_DEBUG, message, arguments);
    }

    /**
     * Append a debug message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void debug(final String message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_DEBUG, message, arguments);
    }

    /**
     * Append an info message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void info(final String message, final Object... arguments) {
        append(Logger.LEVEL_INFO, message, arguments);
    }

    /**
     * Append an info message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void info(final Supplier<String> message, final Object... arguments) {
        append(Logger.LEVEL_INFO, message, arguments);
    }

    /**
     * Append an info message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void info(final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_INFO, message, arguments);
    }

    /**
     * Append an info message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void info(final String message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_INFO, message, arguments);
    }

    /**
     * Append a warning message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void warning(final String message, final Object... arguments) {
        append(Logger.LEVEL_WARNING, message, arguments);
    }

    /**
     * Append a warning message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void warning(final Supplier<String> message, final Object... arguments) {
        append(Logger.LEVEL_WARNING, message, arguments);
    }

    /**
     * Append a warning message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void warning(final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_WARNING, message, arguments);
    }

    /**
     * Append a warning message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void warning(final String message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_WARNING, message, arguments);
    }

    /**
     * Append a severe message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void severe(final String message, final Object... arguments) {
        append(Logger.LEVEL_SEVERE, message, arguments);
    }

    /**
     * Append a severe message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void severe(final Supplier<String> message, final Object... arguments) {
        append(Logger.LEVEL_SEVERE, message, arguments);
    }

    /**
     * Append a severe message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void severe(final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_SEVERE, message, arguments);
    }

    /**
     * Append a severe message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void severe(final String message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_SEVERE, message, arguments);
    }

    /**
     * Append an error message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void error(final String message, final Object... arguments) {
        append(Logger.LEVEL_ERROR, message, arguments);
    }

    /**
     * Append an error message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void error(final Supplier<String> message, final Object... arguments) {
        append(Logger.LEVEL_ERROR, message, arguments);
    }

    /**
     * Append an error message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void error(final Supplier<String> message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_ERROR, message, arguments);
    }

    /**
     * Append an error message
     *
     * @param message the message
     * @param arguments the message arguments
     */
    public void error(final String message, final Supplier<Object[]> arguments) {
        append(Logger.LEVEL_ERROR, message, arguments);
    }
}
