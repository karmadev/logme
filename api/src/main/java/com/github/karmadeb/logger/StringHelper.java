package com.github.karmadeb.logger;

class StringHelper {

    private StringHelper() {}

    public static void formatString(final StringBuilder builder, final Object[] parameters) {
        int index = builder.indexOf("{}");
        int argIndex = 0;
        while (index != -1) {
            boolean scape = isScape(builder, index);
            if (scape) {
                builder.replace(index - 1, index, ""); //Remove scape

                index = builder.indexOf("{}", index + 1);
                continue;
            }

            String replacement = "null";
            if (parameters != null && parameters.length > argIndex)
                replacement = String.valueOf(parameters[argIndex++]);

            builder.replace(index, index + 2, replacement);
            formatSpecificString(builder, replacement, argIndex);

            index = builder.indexOf("{}", index + 1);
        }

        if (parameters != null && argIndex != parameters.length) {
            for (int paramIndex = argIndex; paramIndex < parameters.length; paramIndex++) {
                String replacement = String.valueOf(parameters[paramIndex]);
                formatSpecificString(builder, replacement, paramIndex);
            }
        }
    }

    private static void formatSpecificString(final StringBuilder builder, final String replacement, final int paramIndex) {
        String specificPlaceholder = String.format("{%d}", Math.max(0, paramIndex - 1));
        int index = builder.indexOf(specificPlaceholder);

        while (index != -1) {
            boolean scape = isScape(builder, index);
            if (scape) {
                builder.replace(index - 1, index, ""); //Remove scape

                index = builder.indexOf(specificPlaceholder, index + 1);
                continue;
            }

            builder.replace(index, index + specificPlaceholder.length(), replacement);
            index = builder.indexOf(specificPlaceholder, index + 1);
        }
    }

    private static boolean isScape(final StringBuilder builder, final int index) {
        boolean scaped = false;
        if (index > 0) {
            int m = 1;
            char pre = builder.charAt(index - m++);
            while (pre == '\\') {
                scaped = !scaped;
                int newIndex = index - m++;
                if (newIndex < 0) break;

                pre = builder.charAt(newIndex);
            }
        }

        return scaped;
    }
}