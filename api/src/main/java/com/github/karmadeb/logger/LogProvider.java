package com.github.karmadeb.logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Represents a log provider
 */
@SuppressWarnings("unused")
public abstract class LogProvider {

    /**
     * Create or retrieves a new logger
     * instance
     *
     * @param name the logger name
     * @return the logger
     */
    public Logger getLogger(final String name) {
        return getLogger(Paths.get(name), name);
    }

    /**
     * Get the main logger
     *
     * @return the logger
     */
    public Logger getLogger() {
        return getLogger(Paths.get("logs"));
    }

    /**
     * Get the main logger
     *
     * @param logsContainer the logger file container
     * @return the logger
     */
    public abstract Logger getLogger(final Path logsContainer);

    /**
     * Get the logger for the specified
     * class
     *
     * @param owner the class owning the
     *              logger
     * @return the class logger
     */
    public Logger getLogger(final Class<?> owner) {
        return getLogger(Paths.get("logs"), owner.getSimpleName());
    }

    /**
     * Get the logger for the specified
     * class
     *
     * @param logsContainer the logger file container
     * @param owner the class owning the
     *              logger
     * @return the class logger
     */
    public Logger getLogger(final Path logsContainer, final Class<?> owner) {
        return getLogger(logsContainer, owner.getSimpleName());
    }

    /**
     * Create or retrieves a new logger
     * instance
     *
     * @param logsContainer the logger file container
     * @param name the logger name
     * @return the logger
     */
    public abstract Logger getLogger(final Path logsContainer, final String name);

    /**
     * Get all the existing logger
     * instances
     *
     * @return the logger instances
     */
    public abstract Collection<Logger> getLoggers();

    /**
     * Resolves the level
     *
     * @param level the level
     * @return the level name
     */
    public abstract String resolveLevel(final byte level);
}
