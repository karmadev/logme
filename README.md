# LogMe
LogMe is a lightweight logger library, with support for standalone
java applications and bukkit servers.

<details>
<summary><h3>Adding LogMe via Maven</h3></summary>

LogMe can be added to your maven project through the official repositories

```xml

<repositories>
	<repository>
		<id>gitlab-maven-logme</id>
		<url>https://gitlab.com/api/v4/projects/57430087/packages/maven</url>
	</repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>com.github.karmadeb</groupId>
        <artifactId>logger</artifactId>
        <!-- 
        If you are using bukkit, import this instead:
        <artifactId>bukkit-logger</artifactId> 
        -->
        <version>1.0.0-SNAPSHOT</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```

> Note: It's also highly recommended to shade the library, so it does not
> conflict with other projects

```xml

<plugins>
	<plugin>
		<groupId>org.apache.maven.plugins</groupId>
		<artifactId>maven-shade-plugin</artifactId>
		<version>3.2.4</version>
		<executions>
			<execution>
				<phase>package</phase>
				<goals>
					<goal>shade</goal>
				</goals>
				<configuration>
					<createDependencyReducedPom>false</createDependencyReducedPom>

					<relocations>
						<relocation>
							<pattern>com.github.karmadeb.logger.LoggerComponent</pattern>
							<shadedPattern>com.domain.myproject.logger.LoggerComponent</shadedPattern>
						</relocation>
						<relocation>
							<pattern>com.github.karmadeb.logger</pattern>
							<shadedPattern>com.domain.myproject.logger</shadedPattern>
						</relocation>
					</relocations>
				</configuration>
			</execution>
		</executions>
	</plugin>
</plugins>
```
</details>