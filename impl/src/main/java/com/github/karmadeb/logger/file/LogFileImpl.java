package com.github.karmadeb.logger.file;

import com.github.karmadeb.logger.ConsoleColor;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class LogFileImpl extends LogFile {

    protected final static ScheduledExecutorService EXECUTOR = Executors.newSingleThreadScheduledExecutor();
    protected final static long MAX_FILE_SIZE = 4L * 1024 * 1024 * 1024;

    protected final Path container;
    protected final Path logFile;

    protected GregorianCalendar calendar = GregorianCalendar.from(ZonedDateTime.now());

    /**
     * Create a new log file
     *
     * @param container the log file container
     */
    public LogFileImpl(final Path container) {
        this.container = container;

        logFile = container.resolve("latest.txt");
        if (Files.exists(logFile))
            passDay();

        tryCreateFile(logFile);
    }

    /**
     * Get the file path
     *
     * @return the file path
     */
    @Override
    public Path getFile() {
        GregorianCalendar now = GregorianCalendar.from(ZonedDateTime.now());
        if (now.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)) {
            passDay();
            calendar = now;
        }

        return this.logFile;
    }

    /**
     * Append a line to the log file
     *
     * @param level     the log level
     * @param message   the message
     * @param arguments the message arguments
     */
    @Override
    public void append(final byte level, final String message, final Object... arguments) {
        GregorianCalendar now = GregorianCalendar.from(ZonedDateTime.now());

        EXECUTOR.schedule(() -> {
            if (now.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)) {
                passDay();
                calendar = now;
            }

            if (!Files.exists(this.logFile))
                tryCreateFile(this.logFile);

            try {
                long size = Files.size(this.logFile);
                if (size >= MAX_FILE_SIZE)
                    passDay();

                Files.write(this.logFile, String.format("%s%n", ConsoleColor.strip(message)).getBytes(),
                        StandardOpenOption.APPEND,
                        StandardOpenOption.CREATE);
            } catch (IOException ex) {
                RuntimeException x = new RuntimeException();
                x.addSuppressed(ex);

                throw x;
            }
        }, 0, TimeUnit.MILLISECONDS);
    }

    /**
     * Get the logger component
     * name
     *
     * @return the logger component name
     */
    @Override
    public String getName() {
        String date = getFileDate(0);
        return date.substring(0, date.length() - 2);
    }

    protected void passDay() {
        if (!Files.exists(this.logFile)) return;


        try {
            long size = Files.size(this.logFile);
            if (size <= MAX_FILE_SIZE / 2) return;
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }

        String date = getFileDate(0);
        date = date.substring(0, date.length() - 2);

        Path compressedLogHistory = this.container.resolve(date + ".zip");
        tryCreateZipFile(compressedLogHistory);

        Path in = getInputToZip(compressedLogHistory);
        addToZip(in, compressedLogHistory);
        tryDestroyFile(in);

        tryCreateFile(this.logFile);
    }

    protected String getFileDate(final int count) {
        return String.format("%02d-%02d-%02d-%d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), count);
    }

    protected void tryCreateFile(final Path file) {
        if (Files.exists(file)) return;

        Path parent = file.toAbsolutePath().getParent();
        try {
            if (!Files.exists(parent))
                Files.createDirectories(parent);

            Files.createFile(file);
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }
    }

    protected void tryCreateZipFile(final Path file) {
        if (Files.exists(file)) return;

        Path parent = file.toAbsolutePath().getParent();
        try {
            if (!Files.exists(parent))
                Files.createDirectories(parent);

            try (OutputStream out = Files.newOutputStream(file); ZipOutputStream zOut = new ZipOutputStream(out)) {}
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }
    }

    protected void tryDestroyFile(final Path file) {
        try {
            Files.deleteIfExists(file);
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }
    }

    protected Path getInputToZip(final Path compressedLogHistory) {
        try (ZipFile zip = new ZipFile(compressedLogHistory.toFile())) {
            Enumeration<? extends ZipEntry> entries = zip.entries();
            int count = 1;
            while (entries.hasMoreElements()) {
                count++;
                entries.nextElement();
            }

            String targetDate = getFileDate(count);
            return Files.move(this.logFile, this.container.resolve(targetDate + ".log"));
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }
    }

    protected void addToZip(final Path fileToRead, final Path targetZip) {
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");

        URI uri = URI.create("jar:" + targetZip.toUri());
        try (FileSystem fs = FileSystems.newFileSystem(uri, env)) {
            Path nf = fs.getPath(fileToRead.getFileName().toString());
            try (Writer writer = Files.newBufferedWriter(nf,
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE)) {
                List<String> lines = Files.readAllLines(fileToRead);
                for (String str : lines) {
                    writer.write(str);
                    writer.write('\n');
                }
            }
        } catch (IOException ex) {
            RuntimeException x = new RuntimeException();
            x.addSuppressed(ex);

            throw x;
        }
    }
}