package com.github.karmadeb.logger;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Log provider implementation
 */
public class LogProviderImpl extends LogProvider {

    private final static Map<String, Logger> loggerMap = new ConcurrentHashMap<>();
    private static LoggerImpl impl;

    /**
     * Get the main logger
     *
     * @param logsContainer the logger file container
     * @return the logger
     */
    @Override
    public Logger getLogger(final Path logsContainer) {
        if (LogProviderImpl.impl == null)
            LogProviderImpl.impl = new LoggerImpl(this, logsContainer, "LogProviderImpl");

        return LogProviderImpl.impl;
    }

    /**
     * Create or retrieves a new logger
     * instance
     *
     * @param logsContainer the logger file container
     * @param name          the logger name
     * @return the logger
     */
    @Override
    public Logger getLogger(final Path logsContainer, final String name) {
        return loggerMap.computeIfAbsent(name, (e) -> new LoggerImpl(this, logsContainer, name));
    }

    /**
     * Get all the existing logger
     * instances
     *
     * @return the logger instances
     */
    @Override
    public Collection<Logger> getLoggers() {
        return Collections.unmodifiableCollection(
                LogProviderImpl.loggerMap.values()
        );
    }

    /**
     * Resolves the level
     *
     * @param level the level
     * @return the level name
     */
    @Override
    public String resolveLevel(final byte level) {
        switch (level) {
            case Logger.LEVEL_DEBUG:
                return "DEBUG";
            case Logger.LEVEL_INFO:
                return "INFO";
            case Logger.LEVEL_WARNING:
                return "WARNING";
            case Logger.LEVEL_SEVERE:
                return "SEVERE";
            case Logger.LEVEL_ERROR:
                return "ERROR";
            default:
                return String.format("UNKNOWN-%s", level);
        }
    }
}
