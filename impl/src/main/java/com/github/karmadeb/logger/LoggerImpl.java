package com.github.karmadeb.logger;

import com.github.karmadeb.logger.file.LogFile;
import com.github.karmadeb.logger.file.LogFileImpl;
import com.github.karmadeb.logger.message.LogMessageImpl;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;

/**
 * Logger implementation
 */
public class LoggerImpl extends Logger {

    protected final static GregorianCalendar calendar = GregorianCalendar.from(ZonedDateTime.now());

    protected final Collection<LogMessage> messages = new ConcurrentLinkedQueue<>();

    protected final LogProviderImpl impl;
    protected final LogFileImpl file;
    protected final String name;

    protected byte minLevel = LEVEL_INFO;
    protected String format = "^8[%level-color%%level-name%^r %time%^8] ^6%name%^7:^r %%";
    protected String fileFormat = "[%level-name% %time%] %name%: %%";
    protected Function<LogMessage, LogMessage> loggerFunction;

    /**
     * Create the logger impl
     *
     * @param impl the impl loader
     * @param container the logger file container
     * @param name the logger name
     */
    LoggerImpl(final LogProviderImpl impl, final Path container, final String name) {
        this.impl = impl;
        this.file = new LogFileImpl(container);
        this.name = name;
    }

    /**
     * Create the logger impl
     *
     * @param impl the impl loader
     * @param file the logger file
     * @param name the logger name
     */
    protected LoggerImpl(final LogProviderImpl impl, final LogFileImpl file, final String name) {
        this.impl = impl;
        this.file = file;
        this.name = name;
    }

    /**
     * Set the on-log function of the
     * logger
     *
     * @param consumer the logger consumer
     * @return the logger
     */
    @Override
    protected Logger onLog(Function<LogMessage, LogMessage> consumer) {
        this.loggerFunction = consumer;
        return this;
    }

    /**
     * Get the log message format
     *
     * @return the log message format
     */
    @Override
    public String getLogFormat() {
        return this.format;
    }

    /**
     * Get the log message format when
     * logging into the log file
     *
     * @return the log file format
     */
    @Override
    public String getLogFileFormat() {
        return this.fileFormat;
    }

    /**
     * Get the minimum log level
     *
     * @return the minimum log level
     */
    @Override
    public byte getMinimumLevel() {
        return this.minLevel;
    }

    /**
     * Set the minimum log level
     *
     * @param level the minimum log level
     */
    @Override
    public void setMinimumLevel(final byte level) {
        this.minLevel = level;
    }

    /**
     * Set the log message format
     *
     * @param format the log message format
     * @throws UnsupportedOperationException if the logger implementation
     *                                       does not support a format change
     */
    @Override
    public void setLogFormat(final String format) throws UnsupportedOperationException {
        this.format = format;
    }

    /**
     * Set the log file message format
     *
     * @param format the log file message format
     * @throws UnsupportedOperationException if the logger implementation
     *                                       does not support a format change
     */
    @Override
    public void setLogFileFormat(final String format) throws UnsupportedOperationException {
        this.fileFormat = format;
    }

    /**
     * Log a message
     *
     * @param level     the message level
     * @param message   the message to log
     * @param arguments the message arguments
     */
    @Override
    public void log(final byte level, final String message, final Object... arguments) {
        LogMessage logMessage = new LogMessageImpl(level, message, arguments);
        logMessage(logMessage);
    }

    /**
     * Log a message
     *
     * @param level     the message level
     * @param error     the error
     * @param message   the message to log
     * @param arguments the message arguments
     */
    @Override
    public void log(final byte level, final Throwable error, final String message, final Object... arguments) {
        if (error == null) {
            log(level, message, arguments);
            return;
        }

        String errorMessage = (error.getMessage() == null ? error.fillInStackTrace().toString() : error.getMessage());
        String fnMessage = String.format("%s\n^c%s^r\n%s", message,
                errorMessage, errorToString(error, (element) -> String.format("^3%s^7#^b%s^7(^e%s^7:^6%s^7)",
                        element.getClassName(), element.getMethodName(), element.getFileName(), element.getLineNumber())));

        LogMessage logMessage = new LogMessageImpl(level, error, fnMessage, arguments);
        logMessage(logMessage);
    }

    /**
     * Log a message
     *
     * @param message the message to log
     */
    protected void logMessage(final LogMessage message) {
        LogMessage finalMessage = (this.loggerFunction == null ? message : this.loggerFunction.apply(message));
        String consoleFormatted = this.formatMessage(finalMessage, this.getLogFormat());
        String fileFormatted = this.formatMessage(finalMessage, this.getLogFileFormat());

        if (message.getLevel() >= this.minLevel)
            System.out.println(ConsoleColor.parse(consoleFormatted));

        this.file.append(finalMessage.getLevel(), fileFormatted);
        this.messages.add(finalMessage);
    }

    /**
     * Get the logger attached
     * log file
     *
     * @return the attached log file
     */
    @Override
    public LogFile getAttachedFile() {
        return this.file;
    }

    /**
     * Get the logger logged message
     *
     * @return the logged messages
     */
    @Override
    public Collection<LogMessage> getLogMessages() {
        return Collections.unmodifiableCollection(this.messages);
    }

    /**
     * Flush the log message collection. This method
     * effectively clears all the log messages present
     * in {@link #getLogMessages()}
     */
    @Override
    public void flush() {
        this.messages.clear();
    }

    /**
     * Get the logger component
     * name
     *
     * @return the logger component name
     */
    @Override
    public String getName() {
        return this.name;
    }

    protected String formatMessage(final LogMessage finalMessage, final String format) {
        String gDate = getGregorianDate();
        String date = getDate();
        String time = getTime();
        String level = String.valueOf(finalMessage.getLevel());
        String levelName = impl.resolveLevel(finalMessage.getLevel());
        String levelColor = "^5";

        switch (levelName) {
            case "INFO":
                levelColor = "^e";
                break;
            case "WARNING":
                levelColor = "^a";
                break;
            case "SEVERE":
                levelColor = "^b";
                break;
            case "ERROR":
                levelColor = "^c";
                break;
        }

        return format.replace("%date%", date)
                .replace("%g-date%", gDate)
                .replace("%time%", time)
                .replace("%name%", name)
                .replace("%level-color%", levelColor)
                .replace("%level%", level)
                .replace("%level-name%", levelName)
                .replace("%%", finalMessage.getFormattedMessage());
    }

    protected String getGregorianDate() {
        return String.format("%02d/%02d/%d", calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    }

    protected String getDate() {
        return String.format("%02d/%02d/%d", calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.YEAR));
    }

    protected String getTime() {
        return String.format("%02d:%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
    }
}