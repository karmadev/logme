package com.github.karmadeb.logger.message;

import com.github.karmadeb.logger.LogMessage;

/**
 * Represents a log message
 */
public class LogMessageImpl implements LogMessage {

    private final byte level;
    private final Throwable exception;
    private final String message;
    private final Object[] arguments;

    /**
     * Create a log message
     *
     * @param level the message level
     * @param message the message
     * @param arguments the message arguments
     */
    public LogMessageImpl(final byte level, final String message, final Object... arguments) {
        this(level, null, message, arguments);
    }

    /**
     * Create a log message
     *
     * @param level the message level
     * @param exception the message exception
     * @param message the message
     * @param arguments the message arguments
     */
    public LogMessageImpl(final byte level, final Throwable exception, final String message, final Object... arguments) {
        this.level = level;
        this.exception = exception;
        this.message = (message == null ? "" : message);
        this.arguments = (arguments == null ? new Object[0] : arguments);
    }

    /**
     * Get the logging level
     *
     * @return the logging level
     */
    @Override
    public byte getLevel() {
        return this.level;
    }

    /**
     * Get the logged exception
     *
     * @return the exception
     */
    @Override
    public Throwable getException() {
        return this.exception;
    }

    /**
     * Get the logged message
     *
     * @return the message
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /**
     * Get the message arguments
     *
     * @return the arguments
     */
    @Override
    public Object[] getArguments() {
        return this.arguments;
    }
}
