package com.github.karmadeb.logger;

import com.github.karmadeb.logger.file.LogFile;
import com.github.karmadeb.logger.file.LogFileImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

public class MainTest {

    private LoggerManager manager;
    private LogProvider provider;
    private Logger logger;
    private LogFile logFile;

    @BeforeEach
    public void setup() {
        manager = LoggerManager.getInstance();
        provider = manager.getProvider(LogProviderImpl.class);
        logger = provider.getLogger();
        logFile = logger.getAttachedFile();
    }

    @Test
    public void testLoggerManagerDoesNotConflict() {
        assertDoesNotThrow(LoggerManager::getInstance);
    }

    @Test
    public void testThatLogProviderDoesNotCausesAnyProblem() {
        assertDoesNotThrow(() -> manager.getProvider(LogProviderImpl.class));
    }

    @Test
    public void testThatLogProviderIsALogProviderImpl() {
        assertInstanceOf(LogProviderImpl.class, provider);
    }

    @Test
    public void testThatLogProviderLoggerDoesNotConflict() {
        assertDoesNotThrow(() -> provider.getLogger());
    }

    @Test
    public void testThatLogProviderLoggerIsALoggerImpl() {
        assertInstanceOf(LoggerImpl.class, logger);
    }

    @Test
    public void testThatLogFileDoesNotConflict() {
        assertDoesNotThrow(logger::getAttachedFile);
    }

    @Test
    public void testThatLogFileIsALogFileImpl() {
        assertInstanceOf(LogFileImpl.class, logFile);
    }

    @Test
    public void testThatLoggerLog_and_LoggerFile_works() {
        assertDoesNotThrow(() -> logger.info("This is a log message written to console"));
        assertDoesNotThrow(() -> logFile.info("This is a log message written directly to log file"));
    }
}
